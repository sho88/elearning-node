-- MySQL dump 10.17  Distrib 10.3.14-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: hol
-- ------------------------------------------------------
-- Server version	10.3.14-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Answer`
--

DROP TABLE IF EXISTS `Answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Answer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(11) unsigned DEFAULT NULL,
  `question_id` int(11) unsigned DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_answer_question` (`question_id`),
  CONSTRAINT `c_fk_answer_question_id` FOREIGN KEY (`question_id`) REFERENCES `Question` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Answer`
--

LOCK TABLES `Answer` WRITE;
/*!40000 ALTER TABLE `Answer` DISABLE KEYS */;
INSERT INTO `Answer` VALUES (1,0,1,'You do not need to accept Christ before you can be baptised with the Holy Spirit',0,0),(2,0,1,'John the baptised was baptised with the Holy Spirit',0,0),(3,1,1,'John 3:16 talks about the love of God for his people by giving his only son.',0,0),(4,0,1,'Nebucadnezzar was the second king of Judah',0,0),(5,0,2,'Adoration Confusion Thoughtfulness Supplication',0,0),(6,1,2,'Adoration Confession Prayer Supplication',0,0),(7,0,2,'Affection Concession Thanksgiving Supplication',0,0),(8,0,2,'Application Confession Thanksgiving Supplication',0,0),(9,0,3,'Adam\'s faith in Woman',0,0),(10,0,3,'Eve\'s belief in the serpent\'s word',0,0),(11,0,3,'Lust of the flesh, eyes and pride of life',0,0),(12,1,3,'Man\'s disobedience towards GOD',0,0);
/*!40000 ALTER TABLE `Answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Course`
--

DROP TABLE IF EXISTS `Course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Course` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` int(11) unsigned DEFAULT NULL,
  `updated_at` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Course`
--

LOCK TABLES `Course` WRITE;
/*!40000 ALTER TABLE `Course` DISABLE KEYS */;
INSERT INTO `Course` VALUES (1,'C.O.R.E Series I','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','6 Week duration',1,2017),(2,'C.O.R.E Series II','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','12 Week duration',2017,2017),(4,'CORE Series: Lite','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','4 Week duration',2017,2017),(5,'12 M\'s of Ministry','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','1 Week duration',2017,2017);
/*!40000 ALTER TABLE `Course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CourseEnrol`
--

DROP TABLE IF EXISTS `CourseEnrol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseEnrol` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `course_id` int(11) unsigned DEFAULT NULL,
  `created_at` int(11) unsigned DEFAULT NULL,
  `updated_at` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_courseenroll_user` (`user_id`),
  KEY `index_foreignkey_courseenroll_course` (`course_id`),
  CONSTRAINT `c_fk_courseenroll_course_id` FOREIGN KEY (`course_id`) REFERENCES `Course` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `c_fk_courseenroll_user_id` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CourseEnrol`
--

LOCK TABLES `CourseEnrol` WRITE;
/*!40000 ALTER TABLE `CourseEnrol` DISABLE KEYS */;
INSERT INTO `CourseEnrol` VALUES (1,6,1,2017,2017),(11,NULL,1,2017,2017),(12,NULL,2,2017,2017),(13,NULL,5,2017,2017);
/*!40000 ALTER TABLE `CourseEnrol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EvaluationAnswer`
--

DROP TABLE IF EXISTS `EvaluationAnswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EvaluationAnswer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `eval_question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `eval_question_id` (`eval_question_id`),
  CONSTRAINT `EvaluationAnswer_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`),
  CONSTRAINT `EvaluationAnswer_ibfk_2` FOREIGN KEY (`eval_question_id`) REFERENCES `EvaluationQuestion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EvaluationAnswer`
--

LOCK TABLES `EvaluationAnswer` WRITE;
/*!40000 ALTER TABLE `EvaluationAnswer` DISABLE KEYS */;
/*!40000 ALTER TABLE `EvaluationAnswer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EvaluationQuestion`
--

DROP TABLE IF EXISTS `EvaluationQuestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EvaluationQuestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EvaluationQuestion`
--

LOCK TABLES `EvaluationQuestion` WRITE;
/*!40000 ALTER TABLE `EvaluationQuestion` DISABLE KEYS */;
/*!40000 ALTER TABLE `EvaluationQuestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups`
--

DROP TABLE IF EXISTS `Groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups`
--

LOCK TABLES `Groups` WRITE;
/*!40000 ALTER TABLE `Groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `Groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Invitaiton`
--

DROP TABLE IF EXISTS `Invitaiton`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Invitaiton` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invite_from` int(11) NOT NULL,
  `invite_to` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Invitaiton`
--

LOCK TABLES `Invitaiton` WRITE;
/*!40000 ALTER TABLE `Invitaiton` DISABLE KEYS */;
/*!40000 ALTER TABLE `Invitaiton` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Question`
--

DROP TABLE IF EXISTS `Question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Question` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) unsigned DEFAULT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Question`
--

LOCK TABLES `Question` WRITE;
/*!40000 ALTER TABLE `Question` DISABLE KEYS */;
INSERT INTO `Question` VALUES (1,1,'Which of the following statements is true?',NULL,NULL,0),(2,1,'ACTS is an acryonym used for prayer. What does it stand for?',NULL,NULL,0),(3,1,'What caused the fall of man?',NULL,NULL,0);
/*!40000 ALTER TABLE `Question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Test`
--

DROP TABLE IF EXISTS `Test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Test` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course_id` int(11) unsigned DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_exam_course` (`course_id`),
  CONSTRAINT `c_fk_exam_course_id` FOREIGN KEY (`course_id`) REFERENCES `Course` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Test`
--

LOCK TABLES `Test` WRITE;
/*!40000 ALTER TABLE `Test` DISABLE KEYS */;
INSERT INTO `Test` VALUES (1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla mollis libero nec metus posuere pellentesque. Maecenas ut odio et velit eleifend pretium eget lacinia nunc. \r\n            Praesent feugiat massa eget dui vulputate, aliquet vehicula magna auctor. Maecenas vulputate, quam facilisis lacinia interdum, nisi enim congue ante, eu aliquet nisi elit vitae libero. \r\n            In vitae tortor nibh. Nam ex dui, suscipit non mi ut, vehicula porttitor velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. \r\n            Suspendisse mi lectus, euismod eu facilisis quis, vulputate sed neque. Nam ornare vulputate sapien non pellentesque.',1,NULL,NULL,0);
/*!40000 ALTER TABLE `Test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Topic`
--

DROP TABLE IF EXISTS `Topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Topic` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `topic_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` int(11) unsigned DEFAULT NULL,
  `updated_at` int(11) unsigned DEFAULT NULL,
  `course_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_topic_course` (`course_id`),
  CONSTRAINT `c_fk_topic_course_id` FOREIGN KEY (`course_id`) REFERENCES `Course` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Topic`
--

LOCK TABLES `Topic` WRITE;
/*!40000 ALTER TABLE `Topic` DISABLE KEYS */;
INSERT INTO `Topic` VALUES (1,'Salvation','What we have been saved from, who we have been saved from, and what we have been saved into. What is salvation? Who is the saviour?','osdijfdoinhfdsj fdsfondskj dkfds bfkibfdkidfbdsbf dskhf fkdsjbfdskjfbdskjf dskjf dskfbdsfkbfskdjbfgfdskjbgfjbgfkbgdkfjgdnmf gdfkjbgdfk gfdkjgdkf gfdk gdkfj gdkfbgfdkjbgfdkj hgfdnm hgfdkjbgfdk',1499868065,1499868065,1),(2,'Prayer','Why do we need to pray? What is the purpose of praying?','osdijfdoinhfdsj fdsfondskj dkfds bfkibfdkidfbdsbf dskhf fkdsjbfdskjfbdskjf dskjf dskfbdsfkbfskdjbfgfdskjbgfjbgfkbgdkfjgdnmf gdfkjbgdfk gfdkjgdkf gfdk gdkfj gdkfbgfdkjbgfdkj hgfdnm hgfdkjbgfdk',1499868065,1499868065,1),(3,'Christian Character','What is character? What is personality? What is expected of a believer (in their conduct) etc','osdijfdoinhfdsj fdsfondskj dkfds bfkibfdkidfbdsbf dskhf fkdsjbfdskjfbdskjf dskjf dskfbdsfkbfskdjbfgfdskjbgfjbgfkbgdkfjgdnmf gdfkjbgdfk gfdkjgdkf gfdk gdkfj gdkfbgfdkjbgfdkj hgfdnm hgfdkjbgfdk',1499868065,1499868065,1),(4,'The Presence of God','Talks about the presence of God in our lives. ','randksflakjsdlfajldkfjalskdjflaksdjkflajsldkfjalksdjflkajsdlfjaklsdjflkjasdlkfjasdjfiaosdfiljawijflaksdjlfkasjdflkjasldkfjlakdfhalwuehfjsdaf',2018,2018,5);
/*!40000 ALTER TABLE `Topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserGroup`
--

DROP TABLE IF EXISTS `UserGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserGroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `groups_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `groups_id` (`groups_id`),
  CONSTRAINT `UserGroup_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`),
  CONSTRAINT `UserGroup_ibfk_2` FOREIGN KEY (`groups_id`) REFERENCES `Groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserGroup`
--

LOCK TABLES `UserGroup` WRITE;
/*!40000 ALTER TABLE `UserGroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserGroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserType`
--

DROP TABLE IF EXISTS `UserType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserType` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserType`
--

LOCK TABLES `UserType` WRITE;
/*!40000 ALTER TABLE `UserType` DISABLE KEYS */;
INSERT INTO `UserType` VALUES (1,'admin'),(2,'lecturer'),(3,'student'),(4,'course_leader');
/*!40000 ALTER TABLE `UserType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `church` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` int(11) unsigned DEFAULT NULL,
  `updated_at` int(11) unsigned DEFAULT NULL,
  `usertype_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_user_usertype` (`usertype_id`),
  CONSTRAINT `c_fk_user_usertype_id` FOREIGN KEY (`usertype_id`) REFERENCES `UserType` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'123.jpg','Jake','White','jake.white@gmail.com','rhodesian_ridgeback','Hillsong London','07987342567',1,1,3),(2,NULL,'Ekow','Mensah','yamoah.ensah@yahoo.com','$2y$10$HXUZXMLgGvGsfeq49yRHVe.gaYd/vIuua6A6joh.hUiHwE2yuDhqq','Temple Church','07925602856',NULL,NULL,3),(3,NULL,'Karen','Tay','karentay@yahoo.com','$2y$10$mJzBQiU/2fvwo.Cdcp7/1OIvRbf/tyIiKY5sp97h8EbfpPAnky3Ei','Hillsong London','0789765345',NULL,NULL,3),(4,'','Sho-Silva','Carter-Daniel','shosilvadaniel@gmail.com','$2y$10$f9WYHYBOtwHrbaT9.cPbBuTc6l8OZDutEFwZVeUVJk5LQTLQIKdYS','House of Lights Ministries','07494782612',1,1,1),(5,NULL,'Mo','Carter-Daniel','mo@houseoflights.org.uk','$2y$10$Tzg9oQPGXg1f7QuUSa.Vc.KlOJ4nbTZSxHoFQRi9XHQvRpo8JxVbS','House of Lights Ministries','07999999999',2017,2017,3),(6,NULL,'Tom','Muller','tom_muller@rocketmail.com','$2b$13$2/JK0XQURBAo0m/iU7JtK.1JB.ebrveKYpGTMNDqoICX.z27HR7lq','The Junction Church','07987543234',1554493961,1554493961,3);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Video`
--

DROP TABLE IF EXISTS `Video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video` varchar(30) DEFAULT NULL,
  `video_duration_text` varchar(10) DEFAULT NULL,
  `video_duration` int(11) DEFAULT NULL,
  `topic_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Video`
--

LOCK TABLES `Video` WRITE;
/*!40000 ALTER TABLE `Video` DISABLE KEYS */;
INSERT INTO `Video` VALUES (1,'_jgWmDM0wZw','2m 17s',320,1,1),(2,'9zGLAl-Q1-o','5m 19s',320,2,1),(3,'7l0NZ_TLF3I','6m 08s',320,3,1),(4,'asdfasdfasdf','12m 15s',320,4,5),(7,'_jgWmDM0wZw','2m 17s',320,1,2);
/*!40000 ALTER TABLE `Video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WatchHistory`
--

DROP TABLE IF EXISTS `WatchHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WatchHistory` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `watched_status` int(11) DEFAULT NULL,
  `watched_duration` int(11) DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `video_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WatchHistory`
--

LOCK TABLES `WatchHistory` WRITE;
/*!40000 ALTER TABLE `WatchHistory` DISABLE KEYS */;
INSERT INTO `WatchHistory` VALUES (1,1,320,6,1,1);
/*!40000 ALTER TABLE `WatchHistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (12,1550269635879,'UserTableMigration1550269635879'),(13,1550585884211,'ExamTableMigration1550585884211'),(14,1550591708434,'QuestionTableMigration1550591708434'),(15,1550676102727,'AnswerTableMigration1550676102727'),(16,1550678241661,'TermTableMigration1550678241661'),(17,1550748786578,'TermCourseMigration1550748786578'),(18,1551869976432,'CourseTableMigration1551869976432'),(20,1552170268527,'InvitationTableMigration1552170268527'),(21,1552171478313,'GroupTableMigration1552171478313'),(22,1552172137961,'GroupUserTableMigration1552172137961'),(23,1552306271988,'EvaluationQuestionTable1552306271988'),(24,1552309933298,'EvaluationAnswerTableMigration1552309933298'),(25,1551870834714,'TopicTableMigration1551870834714'),(28,1555692575132,'VideosWatchedTableMigration1555692575132');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'hol'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-17 23:47:29

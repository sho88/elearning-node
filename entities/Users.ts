import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {UserType} from "./UserType";
import {CourseEnrol} from "./CourseEnrol";
import {EvaluationAnswer} from "./EvaluationAnswer";
import {UserGroup} from "./UserGroup";


@Entity("Users",{schema:"hol" } )
@Index("index_foreignkey_user_usertype",["usertype_",])
export class Users {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"photo"
        })
    photo:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"first_name"
        })
    first_name:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"surname"
        })
    surname:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"email_address"
        })
    email_address:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"password"
        })
    password:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"church"
        })
    church:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"telephone_number"
        })
    telephone_number:string;
        

    @Column("int",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:number | null;
    
    @Column("int",{ 
        nullable:false,
        name:"usertype_id"
        })
    usertype_id:number;

   
    @ManyToOne(type=>UserType, userType=>userType.users,{ onDelete: 'SET NULL',onUpdate: 'SET NULL' })
    @JoinColumn({ name:'usertype_id'})
    usertype_:UserType | null;


   
    @OneToMany(type=>CourseEnrol, courseEnrol=>courseEnrol.user_,{ onDelete: 'SET NULL' ,onUpdate: 'SET NULL' })
    courseEnrols:CourseEnrol[];
    

   
    @OneToMany(type=>EvaluationAnswer, evaluationAnswer=>evaluationAnswer.user_,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    evaluationAnswers:EvaluationAnswer[];
    

   
    @OneToMany(type=>UserGroup, userGroup=>userGroup.user_,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    userGroups:UserGroup[];
    
}

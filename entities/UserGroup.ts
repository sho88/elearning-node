import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Users} from "./Users";
import {Groups} from "./Groups";


@Entity("UserGroup",{schema:"hol" } )
@Index("user_id",["user_",])
@Index("groups_id",["groups_",])
export class UserGroup {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

   
    @ManyToOne(type=>Users, users=>users.userGroups,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'user_id'})
    user_:Users | null;


   
    @ManyToOne(type=>Groups, groups=>groups.userGroups,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'groups_id'})
    groups_:Groups | null;

}

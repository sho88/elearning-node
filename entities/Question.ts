import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Answer} from "./Answer";


@Entity("Question",{schema:"hol" } )
export class Question {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("int",{ 
        nullable:true,
        name:"exam_id"
        })
    exam_id:number | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"content"
        })
    content:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:number | null;
        

    @Column("int",{ 
        nullable:false,
        name:"user_id"
        })
    user_id:number;
        

   
    @OneToMany(type=>Answer, answer=>answer.question_,{ onDelete: 'SET NULL' ,onUpdate: 'SET NULL' })
    answers:Answer[];
    
}

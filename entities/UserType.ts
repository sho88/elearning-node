import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Users} from "./Users";


@Entity("UserType",{schema:"hol" } )
export class UserType {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"user_type"
        })
    user_type:string | null;
        

   
    @OneToMany(type=>Users, users=>users.usertype_,{ onDelete: 'SET NULL' ,onUpdate: 'SET NULL' })
    users:Users[];
    
}

import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Question} from "./Question";


@Entity("Answer",{schema:"hol" } )
@Index("index_foreignkey_answer_question",["question_",])
export class Answer {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("int",{ 
        nullable:true,
        name:"status"
        })
    status:number | null;
    
   
    @ManyToOne(type=>Question, question=>question.answers,{ onDelete: 'SET NULL',onUpdate: 'SET NULL' })
    @JoinColumn({ name:'question_id'})
    question_:Question | null;


    @Column("text",{ 
        nullable:false,
        name:"content"
        })
    content:string;
        

    @Column("int",{ 
        nullable:false,
        name:"created_at"
        })
    created_at:number;
        

    @Column("int",{ 
        nullable:false,
        name:"updated_at"
        })
    updated_at:number;

    @Column("int",{ 
        nullable:false,
        name:"question_id"
        })
    question_id:number;
        
}




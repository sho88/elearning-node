import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Users} from "./Users";
import {Course} from "./Course";


@Entity("CourseEnrol",{schema:"hol" } )
@Index("index_foreignkey_courseenroll_user",["user_",])
@Index("index_foreignkey_courseenroll_course",["course_",])
export class CourseEnrol {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

   
    @ManyToOne(type=>Users, users=>users.courseEnrols,{ onDelete: 'SET NULL',onUpdate: 'SET NULL' })
    @JoinColumn({ name:'user_id'})
    user_:Users | null;


   
    @ManyToOne(type=>Course, course=>course.courseEnrols,{ onDelete: 'SET NULL',onUpdate: 'SET NULL' })
    @JoinColumn({ name:'course_id'})
    course_:Course | null;


    @Column("int",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:number | null;
        
}

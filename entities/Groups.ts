import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {UserGroup} from "./UserGroup";


@Entity("Groups",{schema:"hol" } )
export class Groups {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

   
    @OneToMany(type=>UserGroup, userGroup=>userGroup.groups_,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    userGroups:UserGroup[];
    
}

import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import { Course } from "./Course";
import { Topic } from "./Topic";


@Entity("Video",{schema:"hol" } )
export class Video {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:30,
        name:"video"
        })
    video:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:10,
        name:"video_duration_text"
        })
    video_duration_text:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"video_duration"
        })
    video_duration:number | null;
        

    @Column("int",{ 
        nullable:false,
        name:"topic_id"
        })
    topic_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"course_id"
        })
    course_id:number;

    @OneToOne(type => Topic)
    @JoinColumn({name:'topic_id'})
    topic: Topic;

    @ManyToOne(type=>Course, course=>course.topics,{ onDelete: 'CASCADE',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'course_id'})
    course_:Course ;
        
}

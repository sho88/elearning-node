import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {EvaluationAnswer} from "./EvaluationAnswer";


@Entity("EvaluationQuestion",{schema:"hol" } )
export class EvaluationQuestion {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"content"
        })
    content:string;
        

    @Column("int",{ 
        nullable:false,
        name:"created_at"
        })
    created_at:number;
        

    @Column("int",{ 
        nullable:false,
        name:"updated_at"
        })
    updated_at:number;
        

   
    @OneToMany(type=>EvaluationAnswer, evaluationAnswer=>evaluationAnswer.eval_question_,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    evaluationAnswers:EvaluationAnswer[];
    
}

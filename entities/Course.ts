import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {CourseEnrol} from "./CourseEnrol";
import {Test} from "./Test";
import {Topic} from "./Topic";
import { Video } from "./Video";


@Entity("Course",{schema:"hol" } )
export class Course {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"course_name"
        })
    course_name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"description"
        })
    description:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:50,
        name:"duration"
        })
    duration:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:number | null;
        

   
    @OneToMany(type=>CourseEnrol, courseEnrol=>courseEnrol.course_,{ onDelete: 'SET NULL' ,onUpdate: 'SET NULL' })
    courseEnrols:CourseEnrol[];
    

   
    @OneToMany(type=>Test, test=>test.course_,{ onDelete: 'SET NULL' ,onUpdate: 'SET NULL' })
    tests:Test[];
    
    @OneToMany(type=> Video, video=> video.course_, {onDelete: 'CASCADE', onUpdate: 'CASCADE'})
    videos: Video[];
   
    @OneToMany(type=>Topic, topic=>topic.course_,{ onDelete: 'SET NULL' ,onUpdate: 'SET NULL' })
    topics:Topic[];
    
}

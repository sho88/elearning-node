import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("WatchHistory",{schema:"hol" } )
export class WatchHistory {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("int",{ 
        nullable:true,
        name:"watched_status"
        })
    watched_status:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"watched_duration"
        })
    watched_duration:number | null;
        

    @Column("int",{ 
        nullable:false,
        name:"user_id"
        })
    user_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"video_id"
        })
    video_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"course_id"
        })
    course_id:number;
        
}

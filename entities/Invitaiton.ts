import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("Invitaiton",{schema:"hol" } )
export class Invitaiton {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"invite_from"
        })
    invite_from:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"invite_to"
        })
    invite_to:string;
        

    @Column("tinyint",{ 
        nullable:false,
        name:"status"
        })
    status:number;
        
}

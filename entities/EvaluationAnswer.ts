import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Users} from "./Users";
import {EvaluationQuestion} from "./EvaluationQuestion";


@Entity("EvaluationAnswer",{schema:"hol" } )
@Index("user_id",["user_",])
@Index("eval_question_id",["eval_question_",])
export class EvaluationAnswer {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"content"
        })
    content:string;
        

    @Column("int",{ 
        nullable:false,
        name:"created_at"
        })
    created_at:number;

    @Column("int",{ 
        nullable:false,
        name:"eval_question_id"
        })
    eval_question_id:number;
    
    @Column("int",{ 
        nullable:false,
        name:"user_id"
        })
    user_id:number;
        

   
    @ManyToOne(type=>Users, users=>users.evaluationAnswers,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'user_id'})
    user_:Users | null;


   
    @ManyToOne(type=>EvaluationQuestion, evaluationQuestion=>evaluationQuestion.evaluationAnswers,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'eval_question_id'})
    eval_question_:EvaluationQuestion | null;

}

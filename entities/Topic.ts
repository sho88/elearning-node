import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Course} from "./Course";
import { Video } from "./Video";


@Entity("Topic",{schema:"hol" } )
@Index("index_foreignkey_topic_course",["course_",])
export class Topic {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"topic_name"
        })
    topic_name:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"description"
        })
    description:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"content"
        })
    content:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:number | null;
    
    @Column("int",{ 
        nullable:false,
        name:"course_id"
        })
    course_id:number;

   
    @ManyToOne(type=>Course, course=>course.topics,{ onDelete: 'SET NULL',onUpdate: 'SET NULL' })
    @JoinColumn({ name:'course_id'})
    course_:Course | null;

    

}

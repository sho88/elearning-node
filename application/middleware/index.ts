import { AnswerValidation, AnswerValidationCriteria } from './AnswerMiddleware';
import {
    AuthenticateAdmin, AuthenticateCourseLeaderOrAdmin, AuthenticateLecturer, AuthenticateStudent,
    Login, LoginValidationCriteria
} from './AuthMiddleware';
import { CourseValidation, CourseValidationCriteria } from './CourseMiddleware';
import { ExamValidation, ExamValidationCriteria } from './ExamMiddleware';
import { QuestionValidation, QuestionValidationCriteria } from './QuestionMiddleware';
import { TopicValidation, TopicValidationCriteria } from './TopicMiddleware';
import {
    CreateUserMiddlewareCriteria, CreateUserMiddlewareValidation, UserMiddlewareCriteria,
    UserMiddlewareValidation
} from './UserMiddleware';

export default {
  AuthenticateStudent,
  AuthenticateAdmin,
  AuthenticateLecturer,
  AuthenticateCourseLeaderOrAdmin,
  LoginValidationCriteria,
  Login,
  UserMiddlewareCriteria,
  UserMiddlewareValidation,
  CreateUserMiddlewareCriteria,
  CreateUserMiddlewareValidation,
  CourseValidationCriteria,
  CourseValidation,
  TopicValidationCriteria,
  TopicValidation,
  ExamValidationCriteria,
  ExamValidation,
  QuestionValidationCriteria,
  QuestionValidation,
  AnswerValidationCriteria,
  AnswerValidation,
  
};

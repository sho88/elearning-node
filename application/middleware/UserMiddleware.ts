/*------------------------------------------------------------------------------------------
 * @file UserMiddleware.ts
 * @Author House of Lights Web Team
 * @version 1.0.0
 * @date 16th February 2019
 * @brief Middleware controller for handling validaiton of input fields for a user
 *-----------------------------------------------------------------------------------------*/

import * as httpStatus from "http-status-codes";
import { Request, Response, NextFunction } from "express";
import { check, validationResult } from "express-validator/check";

/**
 * Defines the validation criteria when creating a new user
 */
export const CreateUserMiddlewareCriteria = [
  check("photo").optional().isString(),
  check("firstname", "First name cannot be left empty").not().isEmpty(),
  check("surname", "Surname cannot be left empty").exists().not().isEmpty(),
  check("password", "Please provide a password").exists().not().isEmpty(),
  check("email", "Invalid email address").exists().isEmail().not().isEmpty(),
  check("church", "Please enter the name of the church you attend").exists().not().isEmpty(),
  check("telephone", "Telephone number cannot be left empy").exists().not().isEmpty()
];

/**
 * This function handles the validation of the user's input against the user 
 * validation criteria and returns error messages if there are any errors, 
 * otherwise, the next function is called
 *
 * @param req (Request) contains all the information pertaining to the user request
 * @param res  (Response) contains all the necessary data and methods required by the server to send a response
 * @param next (NextFunction) this calls the NextFunction within the function chain 
 */
export const CreateUserMiddlewareValidation = (req: Request, res: Response, next: NextFunction) => {
  
  // check for any validation errors
  const errors = validationResult(req);

  // if any validation errors exist, return the errors to the user as json. 
  if (!errors.isEmpty()) {
    return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({ errors: errors.array() });
  } else {
    return next();
  }
};

/**
 * Responsible for checking whether the env variable exists and what the env variable is 
 * set to.
 */
export const UserMiddlewareCriteria = [
  check('env').exists(),
  check('env').isIn(['dev', 'test', 'prod'])
];

export const UserMiddlewareValidation = (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.json({ errors: errors.array() });
  }
  next();
}

/*------------------------------------------------------------------------------------------
 * @file CourseMiddleware.ts
 * @Author House of Lights Web Team
 * @version 1.0.0
 * @date 22th February 2019
 * @brief Middleware controller for handling validation of input fields for a course
 *-----------------------------------------------------------------------------------------*/

import * as httpStatus from "http-status-codes";
import {Request, Response, NextFunction} from "express"
import {check, validationResult} from "express-validator/check";

/**
 * This defines the validation criteria for a particular course
 */
export const CourseValidationCriteria  =  [
    check("course_name", "Please enter a course name").exists().not().isEmpty().isString(),
    check("description", "A course needs a description").exists().not().isEmpty(),
    check("duration", "A course must have a valid duration").exists().not().isEmpty().isString()
];

/**
 * 
 * This function checks what the user input against the course validation criteria 
 * 
 * @param req (Request) contains all the information pertaining to the user request
 * @param res  (Response) contains all the necessary data and methods required by the server to send a response
 * @param next (NextFunction) this calls the NextFunction within the function chain 
 */
export const CourseValidation = (req: Request, res: Response, next: NextFunction) => {
    
    //check for any errors within input fields based on specified validation criteria
    const errors = validationResult(req);

    // if errors exist return error to user as json 
    if (!errors.isEmpty()) {
        return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({errors: errors.array()})
    }

    return next();
}



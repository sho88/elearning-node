/*------------------------------------------------------------------------------------------
 * @file ExamMiddleware.ts
 * @Author House of Lights Web Team
 * @version 1.0.0
 * @date 22th February 2019
 * @brief Middleware controller for handling validation of input fields for an exam
 *-----------------------------------------------------------------------------------------*/

import * as httpStatus from "http-status-codes";
import { Request, Response, NextFunction } from "express";
import { check, validationResult } from "express-validator/check";

/**
 * Defines the validation criteria for an exam record to ensure the right data is sent to the 
 * server. 
 */
export const ExamValidationCriteria = [
  check("description", "An exam must have a description")
    .exists()
    .not()
    .isEmpty(),
  check("course_id")
    .exists()
    .withMessage("An exam must be linked to a course")
    .isNumeric().withMessage("The Course Id must be a number")
];

/**
 * 
 * This function checks what the user input from the request body against the exam validation criteria 
 * 
 * 
 * @param req (Request) contains all the information pertaining to the user request
 * @param res  (Response) contains all the necessary data and methods required by the server to send a response
 * @param next (NextFunction) this calls the NextFunction within the function chain 
 */
export const ExamValidation = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  
  // check for any validation errors against question validation criteria
  const errors = validationResult(req);
 
  // if any errors are found, return the errors as json to the user with http status code 422.
  if (!errors.isEmpty()) {
    return res
      .status(httpStatus.UNPROCESSABLE_ENTITY)
      .json({ errors: errors.array() });
  }

  return next();
};

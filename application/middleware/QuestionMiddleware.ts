/*------------------------------------------------------------------------------------------
 * @file QuestionMiddleware.ts
 * @Author House of Lights Web Team
 * @version 1.0.0
 * @date 22th February 2019
 * @brief Middleware controller for handling validation of input fields for a question
 *-----------------------------------------------------------------------------------------*/

import * as httpStatus from "http-status-codes";
import {Request, Response, NextFunction} from "express";
import {check, validationResult} from "express-validator/check";

/**
 * This defines the validation criteria for a valid question
 *  
 */
export const QuestionValidationCriteria = [
    check("content", "A question must have content").exists().not().isEmpty()
];

/**
 * 
 * This function checks what the user input against the question validation criteria 
 * 
 * 
 * @param req (Request) contains all the information pertaining to the user request
 * @param res  (Response) contains all the necessary data and methods required by the server to send a response
 * @param next (NextFunction) this calls the NextFunction within the function chain 
 */
export const QuestionValidation = (req: Request, res: Response, next: NextFunction) => {

    // check for any errors within the input data
    const errors = validationResult(req);

    // if errors exist return the errors to the user as json with the http status code 422
    if (!errors.isEmpty()) {
        return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({ errors: errors.array()});
    }
    
    return next();
}
/*------------------------------------------------------------------------------------------
 * @file TopicMiddleware.ts
 * @Author House of Lights Web Team
 * @version 1.0.0
 * @date 22th February 2019
 * @brief Middleware controller for handling validaiton of input fields for a Topic
 *-----------------------------------------------------------------------------------------*/

import { Request, Response, NextFunction } from "express";
import { check, validationResult } from "express-validator/check";

/**
 * This defines the validation criteria used to check the information input
 * by the user and to ensure that it is correct
 */
export const TopicValidationCriteria = [
  check("topic_name")
    .exists()
    .not()
    .isEmpty()
    .withMessage("Please enter a suitable topic name")
    .not()
    .isNumeric()
    .withMessage("Topic name cannot be numeric"),

  check("description", "Please eneter a description for the topic")
    .exists()
    .not()
    .isEmpty(),

  check("content").optional()
];

/**
 * This function handles the validation of the user's input and returns error messages
 * if there are any errors, otherwise, the next function is called
 *
 * @param req (Request) contains all the information pertaining to the user request
 * @param res  (Response) contains all the necessary data and methods required by the server to send a response
 * @param next (NextFunction) this calls the NextFunction within the function chain 
 */
export const TopicValidation = (
  req: Request,
  res: Response,
  next: NextFunction
) => {

  // check for any validation errors
  const errors = validationResult(req);

  // if therre are any validation errors, return the errors as json 
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  } 
  
  return next();
  
};

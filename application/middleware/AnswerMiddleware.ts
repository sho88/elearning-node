/*------------------------------------------------------------------------------------------
 * @file AnswerMiddleware.ts
 * @Author House of Lights Web Team
 * @version 1.0.0
 * @date 22th February 2019
 * @brief Middleware controller for handling validation of input fields for an answer
 *-----------------------------------------------------------------------------------------*/

import * as httpStatus from "http-status-codes";
import { Request, Response, NextFunction } from "express";
import { check, validationResult } from "express-validator/check";

/**
 * This defines the validation criteria for a valid answer
 */
export const AnswerValidationCriteria = [
  check("content", "An answer needs some content")
    .exists()
    .not()
    .isEmpty(),
  check("status", "Status must be a number and can be only be 0 or 1")
    .exists()
    .isNumeric()
    .matches("[0-1]"),
  check("question_id", ["An Answer must be assigned to a question", "Question ID must be a number"])
    .exists()
    .not()
    .isEmpty()
    .isNumeric()
];


/**
 * This function handles the validation of the user's input against the answer 
 * validation criteria and returns error messages if there are any errors, 
 * otherwise, the next function is called
 *
 * @param req (Request) contains all the information pertaining to the user request
 * @param res  (Response) contains all the necessary data and methods required by the server to send a response
 * @param next (NextFunction) this calls the NextFunction within the function chain 
 */
export const AnswerValidation = (req: Request, res: Response, next: NextFunction) => {
  
  // check for any errors within the user input data in the request.
  const errors = validationResult(req);

  // if any errors are found return http status 422 with error as json to the user. 
  if (!errors.isEmpty()) {
    return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({ errors: errors.array() });
  }

  return next();
  
};

/*------------------------------------------------------------------------------------------
 * @file AuthMiddleware.ts
 * @Author House of Lights Web Team
 * @version 1.0.0
 * @date 27th February 2019
 * @brief Middleware controller for handling authentication and authorisation of users
 *-----------------------------------------------------------------------------------------*/

import * as passport from "passport";
import * as LocalStategy from "passport-local";
import * as passportJWT from "passport-jwt";
import * as bcrypt from "bcrypt";
import * as httpStatus from "http-status-codes";
import { AuthService } from "../services/AuthService";
import { Repository } from "typeorm";
import { Request, Response, NextFunction } from "express";
import { check, validationResult } from "express-validator/check";
import { IVerifyOptions } from "passport-local";
import { Users } from "../../entities/Users";
import * as config from "config";



/* Instantiate Auth Service, JWT verification strategy and JWT extraction */
const _authService = new AuthService(new Repository());
const JWTStrategy = passportJWT.Strategy;

/*
====================================================================================
                            PASSSPORT STRATEGIES
====================================================================================
*/

/**
 * This is a definition of a local passport strategy to authenticate all user types
 * on login. It attempts ot first find a user based on the email sent in the request
 * and if the user is not found, an error is returned.
 *
 * @param email (string) contains data passed to the email field.
 * @param password (string) contains the data passed to the password field.
 * @param done (function) this is a callback function which returns an error or a user
 * @returns done (A callback function used by the passport.authenticate function)
 */
passport.use(
  "login",
  new LocalStategy(
    {
      usernameField: "email",
      passwordField: "password"
    },
    async (email: string, password: string, done) => {
      try {
        
        // look for the user using their email
        const user = await _authService.getUserByEmail(email);

        // if the user is not found, return an error
        if (!user) return done(null, false, { error: "Your account does not exist" });

        // compare the hashed password with the password entered by the user for a match
        const validPassword = await bcrypt.compare(password, user.password);

        // if a match is not found return an error message
        if (!validPassword) {
          return done(null, false, { error: "The password entered was invalid" });
        }

        // if email is found and password is valid return the found user.
        return done(null, user);
      } catch (error) {
        // if an error occurs e.g. database connection failed, return the caught error. 
        return done(error);
      }
    }
  )
);


/** 
* This is a JWT Strategy used to verify a JWT that was given to a specific student user.
*  It attempts to find the user based on the data extracted from the token provided in the
*  Authorisation header. if the user exists, then the found user information is passed to 
*  the verify callback function. 
* 
* @param secretOrKey the secretKey used to sign the jwt
* @param jwtFromRequest indicates where to extract the jwt from (e.g. from request body, headers etc.)
* @param payload (object) This contains the information that was extracted from the jwt
* @param done (function) Callback function that is used to verify the jwt.
*/

passport.use(
  "student-auth",
  new JWTStrategy(
    {
      secretOrKey: config.get("jwt.studentSecretOrKey"),
      jwtFromRequest: req => req.signedCookies.jwt
    },
    async (payload, done) => {
      try {

        // look up the user based on the email provided in the jwt
        const validStudent = await _authService.getUserByEmail(payload.email);

        // if the user is not valid return an error to the verify callback
        if (!validStudent) {
          return done(null, { error: "User does not exist" });
        }

        // if the user is found, return true
        return done(null, true, "authorised");
      } catch (error) {
        // if an error occurs return the error 
        return done(error, false);
      }
    }
  )
);

/** 
 * 
 * This is a JWT Strategy used to verify a JWT that was given to a specific admin user.
 *  It attempts to find the user based on the data extracted from the token provided in the
 *  Authorisation header. if the user exists, then the found user information is passed to 
 *  the verify callback function. 
 * 
 * @param secretOrKey the secretKey used to sign the jwt
 * @param jwtFromRequest indicates where to extract the jwt from (e.g. from request body, headers etc.)
 * @param payload (object) This contains the information that was extracted from the jwt
 * @param done (function) Callback function that is used to verify the jwt.
 * 
 */
passport.use(
  "admin-auth",
  new JWTStrategy(
    {
      jwtFromRequest: req => req.signedCookies.jwt,
      secretOrKey: config.get("jwt.courseLeaderAndAdminKey")
    },
    async (payload, done) => {
      try {
        // look up the admin user based on the email field extracted from the jwt
        const validAdmin = await _authService.findAdmin(payload.email);

        // if the admin user cannot be found return an error
        if (!validAdmin) {
          return done(null, false, {
            error:
              "This user is not authorised to view this portion of the website"
          });
        }
        // return true if the admin user is found
        return done(null, true, "authorised");
      } catch (error) {
        // return an error if an error occurs
        return done(error, false);
      }
    }
  )
);


/** 
 * 
 * This is a JWT Strategy used to verify a JWT that was given to a specific lecturer user.
 *  It attempts to find the user based on the data extracted from the token provided in the
 *  Authorisation header. if the user exists, then the found user information is passed to 
 *  the verify callback function. 
 * 
 * @param secretOrKey the secretKey used to sign the jwt
 * @param jwtFromRequest indicates where to extract the jwt from (e.g. from request body, headers etc.)
 * @param payload (object) This contains the information that was extracted from the jwt
 * @param done (function) Callback function that is used to verify the jwt.
 * 
 */
passport.use(
  "lecturer-auth",
  new JWTStrategy(
    {
      secretOrKey: config.get("jwt.lecturerSecretOrKey"),
      jwtFromRequest: req => req.signedCookies.jwt
    },
    async (payload, done) => {
      try {
        // look up the lecturer user based on the email field extracted from the jwt
        const validLecturer = await _authService.findLecturer(payload.email);

        // if the lecturer user cannot be found return an error
        if (!validLecturer) {
          return done(null, false, {
            error:
              "This user is not authorised to view this portion of the website"
          });
        }

        // return the user id if the lecturer user is found
        return done(null, validLecturer[0], "authorised");
      } catch (error) {
        // return an error if an error occurs
        return done(error, false);
      }
    }
  )
);



/**  
*  This is a JWT Strategy used to verify a JWT that was given to a specific course leader or admin user.
*  It attempts to find the user based on the data extracted from the token provided in the
*  Authorisation header. If the user exists, then the found user information is passed to 
*  the verify callback function. 
* 
* @param secretOrKey the secretKey used to sign the jwt
* @param jwtFromRequest indicates where to extract the jwt from (e.g. from request body, headers etc.)
* @param payload (object) This contains the information that was extracted from the jwt
* @param done (function) Callback function that is used to verify the jwt.
* 
*/
passport.use(
  "courseleader-admin-auth",
  new JWTStrategy(
    {
      secretOrKey: config.get("jwt.courseLeaderAndAdminKey"),
      jwtFromRequest: req => req.signedCookies.jwt
    },
    async (payload, done) => {
      try {
        // look up the user based on the email field extracted from the jwt
        const validUser = await _authService.getUserByEmail(payload.email);

        /* check the usertype id to find whether the user is an
         * administrator or course leader if it matches one of the cases
         * return true
         */
        switch (validUser.usertype_id) {
          case 1: {
            return done(null, true, "authorised")
          }
          case 4: {
            return done(null, true, "authorised")
          }

        }

        // otherwise return false
        return done(null, false, {
          error:
            "This user is not authorised to view this portion of the website"
        });
      } catch (error) {
        // return an error if an error occurs
        return done(error, false);
      }
    }
  )
);


/*
====================================================================================
                            LOGIN VALIDATION 
====================================================================================
*/

/** This defines the validation criteria that is applied when a user completes a login form */
export const LoginValidationCriteria = [
  check("email")
    .exists()
    .isEmail()
    .withMessage("Please provide a valid email address")
    .not()
    .isEmpty()
    .withMessage("Email address cannot be empty"),
  check("password")
    .exists()
    .not()
    .isEmpty()
];



/*
====================================================================================
                          PASSSPORT AUTHENTICATION IMPLEMENTATION
====================================================================================
*/



/** 
 * This is a function that performs the validation and authentication of a user 
 * 
 * @param req (Request) this is the request object which contains all the information about the request
 * @param res (Response) this is the response object which contains data and methods pertaning to responses
 * @param next (NextFunction) this is a function which calls the next function in the chain 
 */
export const Login = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  // obtain errors if any from validation based on validation criteria 
  const errors = validationResult(req);

  // loop through errors array and if not empty send errors as json
  if (!errors.isEmpty()) {
    return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({ errors: errors.array() });
  }

  // verify callback function which implements the LocalStrategy "login". 
  passport.authenticate(
    "login",
    { session: false },
    (err: Error, user: Users, info: IVerifyOptions) => {
      // if an error is returned, return the error as json to the user
      if (err) return res.json({ errors: err });

      /* 
       * if any errors are returned from the email or password input by the user,
       * the errors returned are sent back to the user as json with a server
       * status code of 422.
       */
      if (info) {
        return res
          .status(httpStatus.UNPROCESSABLE_ENTITY)
          .json({ error: info.error });
      }

      // add the user id and usertype id to the request body
      req.body.user_id = user.id;
      req.body.usertype_id = user.usertype_id;
      

      return next();
    }
  )(req, res, next);
};

/** 
 * This is a function that performs the verification of a student user's json web token. 
 * 
 * @param req (Request) this is the request object which contains all the information about the request
 * @param res (Response) this is the response object which contains data and methods pertaning to responses
 * @param next (NextFunction) this is a function which calls the next function in the chain 
 */
export const AuthenticateStudent = (
  req: Request,
  res: Response,
  next: NextFunction
) => {

  passport.authenticate(
    "student-auth",
    { session: false },
    (err: Error, student: boolean, info) => {

      /* if any errors are encountered (e.g. an invlaid or expired token)
       * return those errors to the user as json.
       */
      if (err) {
        return res.json({ errors: err });
      }

      /* if a valid student user is not returned by the auth service, 
       * false is returned by the JWTStrategy and an error is sent 
       * to the user as json with the forbidden status code.
       */
      if (!student) {
        return res.status(httpStatus.FORBIDDEN).send({
          error: "You are unauthorised to view this portion of the website",
          message: info.message
        });
      }

      return next();
    }
  )(req, res, next);
};

/**
 * This is a function that performs the verification of an admin user's json web token. 
 * based on the configuration of the JWTStrategy. 
 * 
 * @param req (Request)
 * @param res (Response)
 * @param next (Next Function)
 */
export const AuthenticateAdmin = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  console.log(req)
  passport.authenticate(
    "admin-auth",
    { session: false },
    (err: Error, admin: boolean, info) => {

      /* if any errors are encountered (e.g. an invlaid or expired token)
       * return those errors to the user as json.
       */
      console.log(admin)


      if (err) return res.status(httpStatus.FORBIDDEN).json({ errors: err });

      /* if a valid admin user is not returned by the auth service, 
       * false is returned by the verify callback function and an error is sent 
       * to the user as json with the unauthorized status code.
       */
      if (!admin) {
        return res.status(httpStatus.UNAUTHORIZED).json({
          error: "You are unauthorised to view this portion of the website",
          message: info.message
        });
      }

      return next();
    }
  )(req, res, next);
};


/**
 * This is a function that performs the verification of an lecturer user's json web token. 
 * based on the configuration of the lecturer-auth JWTStrategy. 
 * 
 * @param req (Request)
 * @param res (Response)
 * @param next (Next Function)
 */
export const AuthenticateLecturer = (
  req: Request,
  res: Response,
  next: NextFunction
) => {

  passport.authenticate(
    "lecturer-auth",
    { session: false },
    (err: Error, lecturer:{id}, info) => {

      /* if any errors are encountered (e.g. database connection failure)
       * return those errors to the user as json.
       */

      if (err) return res.status(httpStatus.FORBIDDEN).json({ errors: err });


      /* if a valid lecturer user id is not returned by the auth service, 
       * an empty array is returned by the verify callback function and an error is sent 
       * to the user as json with the unauthorized status code.
       */
      if (!lecturer.id) {
        return res.status(httpStatus.UNAUTHORIZED).json({
          error: "You are unauthorised to view this portion of the website",
          message: info.message
        });
      }

      /* check if the returned lecturer user id matches the user id in the request url.
       * if it does not match, return an error to the user as that user is unauthorised
       */
      if (lecturer.id.toString() !== req.params.userId) {
        return res.status(httpStatus.UNAUTHORIZED).json({
          error: "You are unauthorised to view this portion of the website",
        });
      }

      return next();
    }
  )(req, res, next);
};


/**
 * This is a function that performs the verification of an course leader or admin user's json web token. 
 * based on the configuration of the JWTStrategy. 
 * 
 * @param req (Request)
 * @param res (Response)
 * @param next (Next Function)
 */
export const AuthenticateCourseLeaderOrAdmin = (
  req: Request,
  res: Response,
  next: NextFunction
) => {

  passport.authenticate(
    "courseleader-admin-auth",
    { session: false },
    (err: Error, courseLeaderOrAdmin: boolean, info) => {

      /* if any errors are encountered (e.g. an invlaid or expired token)
       * return those errors to the user as json.
       */
      if (err) return res.json({ errors: err });


      /* if a valid lecturer user is not returned by the auth service, 
       * false is returned by the verify callback function and an error is sent 
       * to the user as json with the unauthorized status code.
       */
      if (!courseLeaderOrAdmin) {
        return res.status(httpStatus.UNAUTHORIZED).json({
          error: "You are unauthorised to view this portion of the website",
          message: info.message
        });
      }

      return next();
    }
  )(req, res, next);
};

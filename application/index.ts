import application from "./boot";
import * as config from "config";

const PORT: string | number = process.env.PORT || config.get("port");

application.listen(PORT, () => console.log(`Listening on PORT: ${PORT}`));

import * as express from "express";
import controllers from "./controllers/index";
import middleware from "./middleware/index";

export const routes = express.Router();


routes
    .post("/api/v1/users/email", controllers.UserController.showByEmailAction)
    .post("/api/v1/login", middleware.LoginValidationCriteria, middleware.Login, controllers.AuthController.loginAction)
    .post('/api/v1/register', middleware.CreateUserMiddlewareCriteria, middleware.CreateUserMiddlewareValidation, controllers.UserController.createAction)
    .get("/api/v1/dashboard/courses", middleware.AuthenticateStudent, controllers.DashboardController.indexAction)
    .get("/admin/dashboard", middleware.AuthenticateAdmin, controllers.DashboardController.adminIndexAction)


    /*------------------------------------------------------------
     *  API ROUTES: The routes that are used to access the data
     *                via the api are defined below
     *-----------------------------------------------------------*/

    /* Users Routes */
    .get("/api/v1/users", middleware.AuthenticateAdmin, controllers.UserController.indexAction)
    .get("/api/v1/users/:userId", middleware.AuthenticateAdmin, controllers.UserController.showAction)
    .post("/api/v1/users", middleware.CreateUserMiddlewareCriteria, middleware.CreateUserMiddlewareValidation, controllers.UserController.createAction)
    .patch("/api/v1/users/:userId", controllers.UserController.updateAction)
    .delete("/api/v1/users/:userId", controllers.UserController.destroyAction)

    /* Courses Routes */
    .get("/api/v1/courses", controllers.CourseController.indexAction)
    .get("/api/v1/courses/:courseId", controllers.CourseController.showAction)
    .get('/api/v1/courses/:courseId/topics', controllers.CourseController.topicsAction)
    .post("/api/v1/courses", middleware.AuthenticateAdmin, middleware.CourseValidationCriteria, middleware.CourseValidation, controllers.CourseController.createAction )
    .patch("/api/v1/courses/:courseId", middleware.AuthenticateAdmin, middleware.CourseValidationCriteria, middleware.CourseValidation, controllers.CourseController.updateAction)
    .delete("/api/v1/courses/:courseId", middleware.AuthenticateAdmin, controllers.CourseController.destroyAction)
    
    /* Topics Routes */
    .get("/api/v1/topics", controllers.TopicController.indexAction)
    .get("/api/v1/topics/:topicId", controllers.TopicController.showAction)
    .get("/api/v1/topics/courses/:courseId", controllers.TopicController.showCourseTopicsAction)
    .post("/api/v1/topics", middleware.AuthenticateAdmin, middleware.TopicValidationCriteria, middleware.TopicValidation, controllers.TopicController.createAction)
    .patch("/api/v1/topics/:topicId", middleware.AuthenticateAdmin, middleware.TopicValidationCriteria, middleware.TopicValidation, controllers.TopicController.updateAction)
    .delete("/api/v1/topics/:topicId", middleware.AuthenticateAdmin, controllers.TopicController.destroyAction)

    /* Questions Routes */
    .get("/api/v1/questions", middleware.AuthenticateCourseLeaderOrAdmin, controllers.QuestionController.indexAction)
    .get("/api/v1/questions/:questionId", middleware.AuthenticateCourseLeaderOrAdmin, controllers.QuestionController.showAction)
    .get("/api/v1/questions/:questionId/answers", middleware.AuthenticateCourseLeaderOrAdmin, controllers.QuestionController.showQuestionAnswersAction)
    .post("/api/v1/questions", middleware.AuthenticateCourseLeaderOrAdmin, middleware.QuestionValidationCriteria, middleware.QuestionValidation, controllers.QuestionController.createAction)
    .patch("/api/v1/questions/:questionId", middleware.AuthenticateCourseLeaderOrAdmin, middleware.QuestionValidationCriteria, middleware.QuestionValidation, controllers.QuestionController.updateAction)
    .delete("/api/v1/questions/:questionId", middleware.AuthenticateCourseLeaderOrAdmin, controllers.QuestionController.destroyAction)

    /* Answers Routes */
    .get("/api/v1/answers", middleware.AuthenticateCourseLeaderOrAdmin, controllers.AnswerController.indexAction)
    .get("/api/v1/answer/:answerId", middleware.AuthenticateCourseLeaderOrAdmin, controllers.AnswerController.showAction)
    .post("/api/v1/answers", middleware.AuthenticateCourseLeaderOrAdmin, middleware.AnswerValidationCriteria, middleware.AnswerValidation, controllers.AnswerController.createAction)
    .patch("/api/v1/answers/:answerId", middleware.AuthenticateCourseLeaderOrAdmin, middleware.AnswerValidationCriteria, middleware.AnswerValidation, controllers.AnswerController.updateAction)
    .delete("/api/v1/answers/:answerId", middleware.AuthenticateCourseLeaderOrAdmin, controllers.AnswerController.destroyAction)

    /* Evaluation Question Routes */
    .get('/api/v1/evaluationquestions', controllers.EvaluationQuestionController.indexAction)
    .get('/api/v1/evaluationquestions/:evaluationQuestionId', controllers.EvaluationQuestionController.showAction)
    .post('/api/v1/evaluationquestions', middleware.AuthenticateAdmin, controllers.EvaluationQuestionController.createAction)
    .patch('/api/v1/evaluationquestions/:evaluationQuestionId', middleware.AuthenticateAdmin, controllers.EvaluationQuestionController.updateAction)
    .delete('/api/v1/evaluationquestions/:evaluationQuestionId', middleware.AuthenticateAdmin, controllers.EvaluationQuestionController.destroyAction)

    /* Evaluation Answer Routes */
    .get('/api/v1/evaluationanswers', controllers.EvaluationAnswerController.indexAction)
    .get('/api/v1/evaluationanswers/:evaluationAnswerId', controllers.EvaluationAnswerController.showAction)
    .post('/api/v1/evaluationanswers', controllers.EvaluationAnswerController.createAction)
    .patch('/api/v1/evaluationanswers/:evaluationAnswerId', controllers.EvaluationAnswerController.updateAction)
    .delete('/api/v1/evaluationanswers/:evaluationAnswerId', controllers.EvaluationAnswerController.destroyAction)

    /* Video Routes */
    .get('/api/v1/videos', controllers.VideoController.indexAction)
    .get('/api/v1/videos/:videoId', controllers.VideoController.showAction)
    .post('/api/v1/videos', controllers.VideoController.createAction)
    .patch('/api/v1/videos/:videoId', controllers.VideoController.updateAction)
    .delete('/api/v1/videos/:videoId', controllers.VideoController.destroyAction)
    
    /* Watch History Routes */
    .get('/api/v1/watchistory/:userId/watched', controllers.WatchHistoryController.showVideosWatchedByUserAction)
    .post('/api/v1/watchistory/watched', middleware.AuthenticateStudent, controllers.WatchHistoryController.updateWatchedStatusAction)

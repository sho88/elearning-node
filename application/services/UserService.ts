import { getConnection, Repository } from "typeorm";
import {Users} from "../../entities/Users";
import * as bcrypt from "bcrypt";
import * as moment from "moment";

export class UserService {

  constructor(private _userRepository: Repository<Users>) { }

  public async initialize() {
    return await getConnection("default");
  }

  public async getUsers() {
    this._userRepository = await getConnection().getRepository(Users);
    return this._userRepository.find();
  }

  public async getUserByEmail(email: string) {
    this._userRepository = await getConnection().getRepository(Users);
    return this._userRepository.findOne({
      select: ["email_address"],
        where: {
          email_address: email
        }
      });
  }

  public async getUsersById(userId: number) {
    this._userRepository = await getConnection().getRepository(Users);
    return this._userRepository.findOneOrFail(userId);
  }


  public async createUser(data: { photo, firstname, surname, email, password, church, telephone }) {
    this._userRepository = await getConnection().getRepository(Users);

    const hashedPassword = await this.storePassword(data.password);

    return await this._userRepository.save({
      photo: data.photo,
      first_name: data.firstname,
      surname: data.surname,
      email_address: data.email,
      password: hashedPassword,
      church: data.church,
      telephone_number: data.telephone,
      created_at: moment().unix(),
      updated_at: moment().unix(),
      usertype_id: 3
    });
  }

  public async storePassword(password: string) {
    return bcrypt.hash(password, await bcrypt.genSalt(13));
  }

  public async editUser(userId: number, data: { photo, firstname, surname, email, password, church, telephone }) {
    this._userRepository = await getConnection().getRepository(Users);

    const hashedPassword = await this.storePassword(data.password);
    return await this._userRepository.update(userId, {
      photo: data.photo,
      first_name: data.firstname,
      surname: data.surname,
      email_address: data.email,
      password: hashedPassword,
      church: data.church,
      telephone_number: data.telephone,
      updated_at: moment().unix()
    });
  }

  public async deleteUser(userId: number) {
    this._userRepository = await getConnection().getRepository(Users);
    return await this._userRepository.delete(userId);
  }
}

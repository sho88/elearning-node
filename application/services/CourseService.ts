import { getConnection, Repository } from "typeorm";
import { Course } from "../../entities/Course";
import * as moment from "moment";
import { Topic } from "../../entities/Topic";

export class CourseService {
  
  constructor(private _courseRepository: Repository<Course>) {}

  public async getAllCourses() {
    this._courseRepository = await getConnection().getRepository(Course);
    return this._courseRepository.find();
  }

  public async getCourseById(courseId: number) {
    this._courseRepository = await getConnection().getRepository(Course);
    return this._courseRepository.findOneOrFail(courseId);
  }
  
  
  public async getVideosByCourseId(courseId: number) {
    this._courseRepository= await getConnection().getRepository(Course);
    return this._courseRepository.find({
      select: ["course_name"],
     join: {
       alias: "courses",
       innerJoinAndSelect: {
         videos: "courses.videos"
       }
     },
      where: {
           id: courseId 
        }      
    })
}

public async getTopicsByCourseId(courseId: number) {
  this._courseRepository = await getConnection().getRepository(Course);
  return this._courseRepository.find({
    select: ["course_name"],
    join: {
      alias: "courses",
      innerJoinAndSelect: {
        topics: "courses.topics"
      }
    },
    where:{ id: courseId }
  })
}

  public async addCourse(data: { course_name; description; duration }) {
    //make connection to database
    this._courseRepository = await getConnection().getRepository(Course);

    //save course entity
    return await this._courseRepository.save({
      course_name: data.course_name,
      description: data.description,
      duration: data.duration,
      created_at: moment().unix(),
      updated_at: moment().unix(),
    });

  }

  public getCourseTopics(courseId: number) {
    return getConnection()
      .getRepository(Topic)
      .find({
        where: [{ course_id: courseId }]
      })
  }

  public async updateCourse(courseId: number, data: { course_name; description; duration }) {
    this._courseRepository = await getConnection().getRepository(Course);

    return await this._courseRepository.update(courseId, {
      course_name: data.course_name,
      description: data.description,
      duration: data.duration,
      updated_at: moment().unix()
    });
  }

  public async deleteCourse(courseId: number) {
    this._courseRepository = await getConnection().getRepository(Course);
    return await this._courseRepository.delete(courseId);
  }
  
}

import { Question } from "../../entities/Question";
import { Repository, getConnection } from "typeorm";
import * as moment from "moment";


export class QuestionService {
    constructor (private _questionRepository: Repository<Question>){}

    public async getAllQuestions() {
        this._questionRepository = await getConnection().getRepository(Question);
        return this._questionRepository.find();
    }

    public async getQuestionById(questionId: number) {
        this._questionRepository = await getConnection().getRepository(Question);
        return this._questionRepository.findOneOrFail(questionId);
    }

    public async createNewQuestion(data: {content, exam_id, user_id}) {
        this._questionRepository = await getConnection().getRepository(Question);
        
        return await this._questionRepository.save({
            content: data.content,
            exam_id: data.exam_id,
            user_id: data.user_id,
            created_at: moment().unix(),
            updated_at: moment().unix()
        });
    }

    public async updateQuestion(questionId: number, data: {content, exam_id, user_id}) {
        this._questionRepository = await getConnection().getRepository(Question);

        return await this._questionRepository.update(questionId, {
            content: data.content,
            exam_id: data.exam_id,
            user_id: data.user_id,
            updated_at: moment().unix()
        });
    } 

    public async deleteQuestion(questionId: number) {
        this._questionRepository = await getConnection().getRepository(Question);
        return this._questionRepository.delete(questionId);
    }
}
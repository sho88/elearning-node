import { Topic } from "../../entities/Topic";
import { getConnection, Repository } from "typeorm";
import * as moment from "moment";

export class TopicService {
  constructor(private _topicRepository: Repository<Topic>) {}

  public async getAllTopics() {
    this._topicRepository = await getConnection().getRepository(Topic);
    return this._topicRepository.find();
  }

  public async getLectuersTopics(lecturerId: number) {
    this._topicRepository = await getConnection().getRepository(Topic);
    return this._topicRepository.query(`
      SELECT topic_name from topic
      INNER JOIN user on user.id = topic.user_id
      WHERE user.id = ${lecturerId};`);
  }

  public async getTopicById(topicId: number) {
    this._topicRepository = await getConnection().getRepository(Topic);
    return this._topicRepository.findOneOrFail(topicId);
  }

  public async getTopicsByCourseId(courseId: number) {
    this._topicRepository = await getConnection().getRepository(Topic);
    return this._topicRepository.createQueryBuilder("Topic")
      .select(["id", "topic_name", "content", "course_id"])
      .where("course_id = :_courseId", {_courseId : courseId} )
  }

  public async createTopic(data: {topic_name, description, content, course_id, user_id, video}) {

    this._topicRepository = await getConnection().getRepository(Topic);

    return await this._topicRepository.save({
        topic_name: data.topic_name,
        description: data.description,
        content: data.content,
        created_at: moment().unix(),
        updated_at: moment().unix(),
        course_id: data.course_id,
        user_id: data.user_id,
        video: data.video
    });

  }

  public async updateTopic(topicId: number, data: { topic_name; description; content; course_id, user_id, video }) {
    this._topicRepository = await getConnection().getRepository(Topic);

    return this._topicRepository.update(topicId, {
      topic_name: data.topic_name,
      description: data.description,
      content: data.content,
      course_id: data.course_id,
      updated_at: moment().unix(),

    });
  }

  public async deleteTopic(topicId: number) {
    this._topicRepository = await getConnection().getRepository(Topic);
    return await this._topicRepository.delete(topicId);
  }

}

import { getConnection, Repository} from "typeorm";
import { Answer } from "../../entities/Answer";
import * as moment from "moment";

export class AnswerService {
  public constructor( private _answerRepository: Repository<Answer>) {}

  public async getAllAnswers() {
    this._answerRepository = await getConnection().getRepository(Answer);
    return this._answerRepository.find();
  }

  public async getAnswerById(answerId: number) {
    this._answerRepository = await getConnection().getRepository(Answer);
    return this._answerRepository.findOneOrFail(answerId);
  }

  public async getAnswersByQuestionId(questionId: number) {
    this._answerRepository = await getConnection().getRepository(Answer);
    return await this._answerRepository.find({
      where: { question_id: questionId }
    });
  }

  public async createNewAnswer(data: { content; status; question_id }) {
    this._answerRepository = await getConnection().getRepository(Answer);

    return await this._answerRepository.save({
      content: data.content,
      status: data.status,
      created_at: moment().unix(),
      updated_at: moment().unix(),
      question_id: data.question_id
    });
  }

  public async updateAnswer(answerId: number, data: { content; status; question_id }) {
      this._answerRepository = await getConnection().getRepository(Answer);

      return await this._answerRepository.update(answerId, {
          content: data.content,
          status: data.status,
          updated_at: moment().unix(),
          question_id: data.question_id
      });
  }

  public async deleteAnswer(answerId: number) {
      this._answerRepository = await getConnection().getRepository(Answer);
      return this._answerRepository.delete(answerId);
  }

}

import * as moment from "moment"
import { Repository, getConnection } from "typeorm";
import { EvaluationAnswer } from "../../entities/EvaluationAnswer";

export class EvaluationAnswerService {

    public constructor(private _evaluationAnswerRepository: Repository<EvaluationAnswer>) { }

    public async getEvaluationAnswers() {
        this._evaluationAnswerRepository = await getConnection().getRepository(EvaluationAnswer);
        return this._evaluationAnswerRepository.find();
    }

    public async getEvaluaionAnswerById(evaluationAnswerId: number) {
        this._evaluationAnswerRepository = await getConnection().getRepository(EvaluationAnswer);
        return this._evaluationAnswerRepository.findOneOrFail(evaluationAnswerId);
    }

    public async addEvaluationAnswer(data: { content, evaluationQuestionId, userId }) {
        this._evaluationAnswerRepository = await getConnection().getRepository(EvaluationAnswer);

        return await this._evaluationAnswerRepository.save({
            content: data.content,
            created_at: moment().unix(),
            updated_at: moment().unix(),
            eval_question_id: data.evaluationQuestionId,
            user_id: data.userId
        });
    }

    public async updateEvaluationAnswer(evaluationAnswerId, data: { content, evaluationQuestionId, userId }) {
        this._evaluationAnswerRepository = await getConnection().getRepository(EvaluationAnswer);

        return await this._evaluationAnswerRepository.update(evaluationAnswerId, {
            content: data.content,
            eval_question_id: data.evaluationQuestionId,
            user_id: data.userId
        });
    }

    public async deleteEvaluationAnswer(evaluationAnswerId) {
        this._evaluationAnswerRepository = await getConnection().getRepository(EvaluationAnswer);
        
        return this._evaluationAnswerRepository.delete(evaluationAnswerId);
    }
}
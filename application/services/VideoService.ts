import { Repository, getConnection, ObjectLiteral } from "typeorm";
import { Video } from "../../entities/Video";

export class VideoService {
    
    public constructor(private _videoRepository: Repository<Video>) {}

    public async getAllVidoes() {
        this._videoRepository = getConnection().getRepository(Video);
        return this._videoRepository.find();
    }

    public async getVideoById(videoId: string) {
        this._videoRepository = getConnection().getRepository(Video);
        return this._videoRepository.findOneOrFail(videoId);
    }

    public async getVideoByCourseId(courseId: number) {
        this._videoRepository = getConnection().getRepository(Video);
        return this._videoRepository.find({where: {course_id: courseId }});
    }

    public async addVideo(data: ObjectLiteral) {
        this._videoRepository = getConnection().getRepository(Video);
        return this._videoRepository.save({
            video: data.video,
            video_duration_text: data.videoDurationText,
            video_duration: data.videoDuration,
            course_id: data.courseId,
            topic_id: data.topicId
        });
    }

    public async updateVideo(videoId: number, data: ObjectLiteral) {
        this._videoRepository = getConnection().getRepository(Video);
        return this._videoRepository.update(videoId, {
            video: data.video,
            video_duration_text: data.videoDurationText,
            video_duration: data.videoDuration,
            course_id: data.courseId,
            topic_id: data.topicId
        });
    }

    public async deleteViideo(videoId: number) {
        this._videoRepository = getConnection().getRepository(Video);
        return this._videoRepository.delete(videoId);
    }
}
import { WatchHistory } from "../../entities/WatchHistory";
import { Repository, getConnection, ObjectLiteral } from "typeorm";


export class WatchHistoryService {
    public constructor(private _WatchHistory: Repository<WatchHistory>) {

    }

    public async getAllVideosWatched() {
        this._WatchHistory = await getConnection().getRepository(WatchHistory);
        this._WatchHistory.find();
    }

    public async getTopicsWatchedByUser(userId: number) {

        this._WatchHistory = await getConnection().getRepository(WatchHistory);
        return this._WatchHistory.find({
            where: [{user_id: userId}]
        })
    }

    public async updateWatchedStatus(data:ObjectLiteral) {
        this._WatchHistory = await getConnection().getRepository(WatchHistory);
        return this.findExistingWatchedRecord(data)
        .then(existingRecord => {
            console.info('upating exising watch record');
            this._WatchHistory.update(existingRecord.id, {
                watched_status: 1,
                watched_duration: data.watched_duration,
                course_id: data.courseId,
                user_id: data.userId,
                video_id: data.videoId
            })

        }).catch(() => {
            console.info('no record exists saving new record...');
            this._WatchHistory.save({
                watched_status: 1,
                watched_duration: data.watched_duration,
                course_id: data.courseId,
                user_id: data.userId,
                video_id: data.videoId
            })
        })

    }


    // ====================== HELPER METHODS ================================= 

    public async findExistingWatchedRecord(data) {
        this._WatchHistory = await getConnection().getRepository(WatchHistory);
        return this._WatchHistory.findOneOrFail({
            where: [
                { 
                    course_id: data.courseId, 
                    user_id: data.userId, 
                }
            
            ]
        })
    }

}
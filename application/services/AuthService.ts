import * as moment from "moment";
import * as jwt from "jsonwebtoken";
import { getConnection, Repository } from "typeorm";
import { Users } from "../../entities/Users";
import * as config from "config";

export class AuthService {
  public constructor(private _userRepository: Repository<Users>) { }

  public async getUserByEmail(_email: string) {
    this._userRepository = await getConnection().getRepository(Users);
    return await this._userRepository.findOneOrFail({
      where: { email_address: _email }
    });
  }

  public async createToken(data: { email; user_id; usertype_id; }) {
    let secretOrKey: string = "";
    const expiresIn: number = moment.utc({ days: 1 }).unix();
    switch (data.usertype_id) {
      case 1: {
        secretOrKey = config.get("jwt.courseLeaderAndAdminKey");
        break;
      }
      case 2: {
        secretOrKey = config.get("jwt.lecturerSecretOrKey");
        break;
      }
      case 3: {
        secretOrKey = config.get("jwt.studentSecretOrKey");
        break;
      }
      case 4: {
        secretOrKey = config.get("jwt.courseLeaderAndAdminKey");
        break;
      }
    }

    const _user = { email: data.email, user_id: data.user_id, usertype_id: data.usertype_id };
    const token = jwt.sign(_user, secretOrKey, { expiresIn });
    return await { expires_in: expiresIn, token, _user };
  }

  public findAdmin(_email: string) {
    return getConnection()
      .getRepository(Users)
      .find({
        where: [{ email_address: _email, usertype_id: 1 }]
      })
  }

  public async findLecturer(_email: string) {
    return getConnection()
    .getRepository(Users)
    .find({
      where: [{email_address: _email, usertype_id: 2}]
    })
  }


  public async getUsersById(userId: number) {
    this._userRepository = await getConnection().getRepository(Users);
    return await this._userRepository.findOneOrFail({ where: { id: userId } });
  }


}

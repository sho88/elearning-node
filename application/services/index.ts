import {UserService} from "./UserService";
import { Repository } from "typeorm";
import { AuthService } from "./AuthService";
import { AnswerService } from "./AnswerService";
import { CourseService } from "./CourseService";
import { QuestionService } from "./QuestionService";
import { TopicService } from "./TopicService";
import { EvaluationQuestionService } from "./EvaluationQuestionService";
import { EvaluationAnswerService } from "./EvaluationAnswerService";
import { WatchHistoryService } from "./WatchHistoryService";
import { VideoService } from "./VideoService";

export default {
  AuthService: new AuthService(new Repository()),
  AnswerService: new AnswerService(new Repository()),
  CourseService: new CourseService(new Repository()),
  EvaluationQuestionService: new EvaluationQuestionService(new Repository()),
  EvaluationAnswerService: new EvaluationAnswerService(new Repository()),
  QuestionService: new QuestionService(new Repository()),
  TopicService: new TopicService(new Repository()),
  UserService: new UserService(new Repository()),
  VideoService: new VideoService(new Repository()),
  WatchHistoryService: new WatchHistoryService(new Repository())

};

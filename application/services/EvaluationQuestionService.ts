import * as moment from "moment";
import { Repository, getConnection } from "typeorm";
import { EvaluationQuestion } from "../../entities/EvaluationQuestion";

export class EvaluationQuestionService {

    public constructor(private _evaluationQuestionRepository: Repository<EvaluationQuestion>) {}

    public async getAllEvaluationQuestions() {
        this._evaluationQuestionRepository = await getConnection().getRepository(EvaluationQuestion);
        return await this._evaluationQuestionRepository.find();
    }

    public async getEvaluationQuestionById(questionId: number) {
        this._evaluationQuestionRepository = await getConnection().getRepository(EvaluationQuestion);
        return await this._evaluationQuestionRepository.findOneOrFail(questionId);
    }

    public async addNewEvaluationQuestion(data: {content}) {
        this._evaluationQuestionRepository = await getConnection().getRepository(EvaluationQuestion);

        return await this._evaluationQuestionRepository.save({
            content: data.content,
            created_at: moment().unix(),
            updated_at: moment().unix()
        });
    }

    public async updateQuestion (questionId:number, data:{content}) {
        this._evaluationQuestionRepository = await getConnection().getRepository(EvaluationQuestion);

        return await this._evaluationQuestionRepository.update(questionId, {
            content: data.content,
            updated_at: moment().unix()
        })
    }

    public async deleteEvaluationQuestion(questionId:number) {
        this._evaluationQuestionRepository = await getConnection().getRepository(EvaluationQuestion);

        return await this._evaluationQuestionRepository.delete(questionId);
    }
}


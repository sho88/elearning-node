import { Request, Response } from "express";
import { UserService } from "../services/UserService";
import * as httpStatus from "http-status-codes";
import { AuthService } from "../services/AuthService";


export class UserController {
  constructor(private _userService: UserService, private _authService: AuthService) {
    this.indexAction = this.indexAction.bind(this);
    this.showAction = this.showAction.bind(this);
    this.showByEmailAction = this.showByEmailAction.bind(this);
    this.createAction = this.createAction.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.destroyAction = this.destroyAction.bind(this);
    this.findAdminAction = this.findAdminAction.bind(this);
  }

  public indexAction(req: Request, res: Response) {
    return this._userService
      .getUsers()
      .then(users => res.status(200).send({ data: users }))
      .catch(err => res.send(err));
  }

  public showAction(req: Request, res: Response) {
    return this._userService
      .getUsersById(req.params.userId)
      .then(user => res.send({ user }))
      .catch(err => res.send({ err }));
  }

  public showByEmailAction(req: Request, res: Response) {
    return this._userService
      .getUserByEmail(req.body.email)
      .then(email => {
        res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
        res.setHeader('Access-Control-Allow-Methods', 'POST');
        res.status(httpStatus.OK).json(email);
      })
      .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err))
  }


  public createAction(req: Request, res: Response) {
    return this._userService
      .createUser(req.body)
      .then(user => {
        this._authService.createToken({email: req.body.email, user_id: user.id, usertype_id: user.usertype_id})
        .then(user => {
          res.status(httpStatus.CREATED).json({ user: user, message: "Account Created Successfully" })
      }).catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));    
  }).catch(err => res.status(httpStatus.UNPROCESSABLE_ENTITY).json(err))
}

  public updateAction(req: Request, res: Response) {
    return this._userService
      .editUser(req.params.userId, req.body)
      .then(user => res.send(user))
      .catch(err => res.send(err));
  }

  public destroyAction(req: Request, res: Response) {
    return this._userService
      .deleteUser(req.params.userId)
      .then(() => res.send("User has been deleted successfully"))
      .catch(err => res.send(err));
  }

  public findAdminAction(req: Request, res: Response) {
    return this._authService.findAdmin(req.body.email)
    .then(admin => res.json(admin))
    .catch(err => res.json(err))
  }
}

import {Request, Response} from "express";
import {TopicService} from "../services/TopicService";

export class TopicController {

    constructor(private _topicService: TopicService) {
        this.indexAction = this.indexAction.bind(this);
        this.showAction = this.showAction.bind(this);
        this.showCourseTopicsAction = this.showCourseTopicsAction.bind(this);
        this.createAction  = this.createAction.bind(this);
        this.updateAction = this.updateAction.bind(this);
        this.destroyAction = this.destroyAction.bind(this);
    }

    public indexAction(req: Request, res: Response) {
       return this._topicService.getAllTopics()
        .then(topics => res.send(topics))
        .catch(err => res.send(err));
    }

    public showAction(req: Request, res: Response) {
        return this._topicService.getTopicById(req.params.topicId)
        .then(topic => res.send(topic))
        .catch(err => res.send(err));
    }

    public showCourseTopicsAction(req: Request, res: Response) {
        return this._topicService.getTopicsByCourseId(req.params.courseId)
        .then(courseTopics => res.send(courseTopics))
        .catch(err => res.send(err));
    }

    public createAction(req: Request, res: Response) {
        return this._topicService.createTopic(req.body)
        .then(msg => res.send(msg))
        .catch(err => res.send(err));
    }

    public updateAction(req: Request, res: Response) {
        return this._topicService.updateTopic(req.params.topicId, req.body)
        .then(msg => res.send(msg))
        .catch(err => res.send(err));
    }

    public destroyAction(req: Request, res: Response) {
        return this._topicService.deleteTopic(req.params.topicId)
        .then(msg => res.send(msg))
        .catch(err => res.send(err));
    }
}
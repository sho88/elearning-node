import { Request, Response } from "express";
import {UserService} from "../services/UserService";

export class IndexController {
  constructor(private _userService: UserService) {
    this.indexAction = this.indexAction.bind(this);
    this.testAction = this.testAction.bind(this);
    this.usersAction = this.usersAction.bind(this);
  }

  indexAction(req: Request, res: Response) {
    return res.render('index', { name: 'eLearning Application' });
  }

  testAction(req: Request, res: Response) {
    return this._userService.getUsersById(req.params.id)
      .then(user => res.send({ user }))
      .catch(err => res.send({ err }));
  }

  usersAction(req, res) {
    return this._userService.getUsers()
      .then(users => res.send(users))
      .catch(err => res.send(err));
  }
}

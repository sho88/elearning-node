import * as httpStatus from "http-status-codes";
import { Request, Response } from "express";
import { CourseService } from "../services/CourseService";
export class CourseController {
  public constructor(private _courseService: CourseService) {
    this.indexAction = this.indexAction.bind(this);
    this.showAction = this.showAction.bind(this);
    this.showVideosByCourseId = this.showVideosByCourseId.bind(this);
    this.showTopicsByCourseId = this.showTopicsByCourseId.bind(this);
    this.createAction = this.createAction.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.destroyAction = this.destroyAction.bind(this);
    this.topicsAction = this.topicsAction.bind(this);
  }

  public indexAction(req: Request, res: Response) {
    return this._courseService
      .getAllCourses()
      .then(courses => res.send(courses))
      .catch(err => res.send(err));
  }

  public showAction(req: Request, res: Response) {
    return this._courseService
      .getCourseById(req.params.courseId)
      .then(course => res.send(course))
      .catch(err => res.send(err));
  }

  public showTopicsByCourseId(req: Request, res: Response) {
    this._courseService
      .getTopicsByCourseId(req.params.courseId)
      .then(topics => res.status(httpStatus.OK).json(topics))
      .catch(error => res.send(error));
  }

  public showVideosByCourseId(req: Request, res: Response) {
    this._courseService
      .getVideosByCourseId(req.params.courseId)
      .then(videos => res.status(httpStatus.OK).json(videos))
      .catch(error => res.send(error));
  }

  public createAction(req: Request, res: Response) {
    return this._courseService
      .addCourse(req.body)
      .then(() => res.status(httpStatus.CREATED).json("Course created successfully"))
      .catch(err => res.send(err));
  }

  public updateAction(req: Request, res: Response) {
    return this._courseService
      .updateCourse(req.params.courseId, req.body)
      .then(() => res.status(httpStatus.OK).json("Course updated successfully"))
      .catch(err => res.send(err));
  }

  public destroyAction(req: Request, res: Response) {
    return this._courseService
      .deleteCourse(req.params.courseId)
      .then(msg => res.send(msg))
      .catch(err => res.send(err));
  }

  public topicsAction(req: Request, res: Response) {
    return this._courseService
      .getCourseTopics(req.params.courseId)
      .then(topics => res.send(topics))
      .catch(err => res.json(err));
  }
}

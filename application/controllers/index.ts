import { AuthController } from "./AuthController";
import {IndexController} from "./IndexController";
import {UserController} from "./UserController";
import {CourseController} from "../controllers/CourseController"
import { TopicController } from "./TopicController";
import {ExamController} from "../controllers/ExamController"
import {QuestionController} from "../controllers/QuestionController";
import { AnswerController } from "./AnswerController";
import { DashboardController } from "./DashboardController";
import services from "../services/index";
import { EvaluationQuestionController } from "./EvaluationQuestionController";
import { EvaluationAnswerController } from "./EvaluationAnswerController";
import { VideoController } from "./VideoController";
import { WatchHistoryController } from "./WatchHistoryController";

export default {
  AuthController: new AuthController(services.AuthService),
  DashboardController: new DashboardController(services.CourseService),
  IndexController: new IndexController(services.UserService),
  UserController: new UserController(services.UserService, services.AuthService),
  CourseController: new CourseController(services.CourseService),
  TopicController: new TopicController(services.TopicService),
  QuestionController: new QuestionController(services.QuestionService, services.AnswerService),
  AnswerController: new AnswerController(services.AnswerService),
  EvaluationQuestionController: new EvaluationQuestionController(services.EvaluationQuestionService),
  EvaluationAnswerController: new EvaluationAnswerController(services.EvaluationAnswerService),
  VideoController: new VideoController(services.VideoService),
  WatchHistoryController: new WatchHistoryController(services.WatchHistoryService)
  
};

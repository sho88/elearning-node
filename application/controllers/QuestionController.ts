import * as httpStatus from "http-status-codes";
import { Request, Response} from "express";
import { QuestionService } from "../services/QuestionService";
import { AnswerService} from "../services/AnswerService";


export class QuestionController {
    public constructor (private _questionService: QuestionService, private _answerService: AnswerService) {
        this.indexAction = this.indexAction.bind(this);
        this.showAction = this.showAction.bind(this);
        this.showQuestionAnswersAction = this.showQuestionAnswersAction.bind(this);
        this.createAction = this.createAction.bind(this);
        this.updateAction = this.updateAction.bind(this);
        this.destroyAction = this.destroyAction.bind(this);
    }

    public indexAction(req: Request, res: Response) {
        return this._questionService.getAllQuestions()
        .then(questions => res.status(httpStatus.OK).json(questions))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public showAction(req: Request, res: Response) {
        return this._questionService.getQuestionById(req.params.questionId)
        .then(question => res.status(httpStatus.OK).json(question))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public showQuestionAnswersAction(req: Request, res: Response) {
        return this._answerService.getAnswersByQuestionId(req.params.questionId)
        .then(answers => res.status(httpStatus.OK).json(answers))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }


    public createAction(req: Request, res: Response) {
        return this._questionService.createNewQuestion(req.body)
        .then(() => res.status(httpStatus.CREATED).json({msg: "Question: " + req.body.content + " was created successfully"}))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public updateAction(req: Request, res: Response) {
        return this._questionService.updateQuestion(req.params.questionId, req.body)
        .then(() => res.status(httpStatus.OK).json({msg: "Question: " + req.body.content + " was updated successfully"}))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));

    }

    public destroyAction(req: Request, res: Response) {
        return this._questionService.deleteQuestion(req.params.questionId)
        .then(() => res.status(httpStatus.OK).json({msg: "Question: " + req.body.content + " was deleted successfully"}))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }
}
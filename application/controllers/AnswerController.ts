import * as httpStatus from "http-status-codes";
import { AnswerService } from "../services/AnswerService";
import { Request, Response } from "express";

export class AnswerController {

    public constructor(private _answerService: AnswerService) {
        this.indexAction = this.indexAction.bind(this);
        this.showAction = this.showAction.bind(this);
        this.createAction = this.createAction.bind(this);
        this.updateAction = this.updateAction.bind(this);
        this.destroyAction = this.destroyAction.bind(this);
    }

    public indexAction(req: Request, res: Response) {
        return this._answerService.getAllAnswers()
        .then(answers => res.status(httpStatus.OK).json(answers))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public showAction(req: Request, res: Response) {
        return this._answerService.getAnswerById(req.params.answerId)
        .then(answer => res.status(httpStatus.OK).json(answer))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public createAction(req: Request, res: Response) {
        return this._answerService.createNewAnswer(req.body)
        .then(() => res.status(httpStatus.CREATED).json({msg: "Answer: " + req.body.content + " was created successfully"}))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public updateAction(req: Request, res: Response) {
        return this._answerService.updateAnswer(req.params.answerId, req.body)
        .then(() => res.status(httpStatus.OK).json({msg: "Answer: " + req.body.content + " was updated successfully"}))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public destroyAction(req: Request, res: Response) {
        return this._answerService.deleteAnswer(req.params.answerId)
        .then(() => res.status(httpStatus.OK).json({msg: "Answer: " + req.body.content + " was deleted successfully"}))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }
}
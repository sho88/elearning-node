import * as httpStatus from "http-status-codes";
import { Request, Response } from "express";
import { EvaluationQuestionService } from "../services/EvaluationQuestionService";
import { EvaluationQuestion } from "../../entities/EvaluationQuestion";


export class EvaluationQuestionController {

    constructor(private readonly _evaluationQuestionService: EvaluationQuestionService) {
        this.indexAction = this.indexAction.bind(this);
        this.showAction = this.showAction.bind(this);
        this.createAction = this.createAction.bind(this);
        this.updateAction = this.updateAction.bind(this);
        this.destroyAction = this.destroyAction.bind(this);
    }

    public indexAction(req: Request, res: Response) {
        return this._evaluationQuestionService.getAllEvaluationQuestions()
            .then(evaluationQuestions => res.status(httpStatus.OK).json(evaluationQuestions))
            .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public showAction(req: Request, res: Response) {
        return this._evaluationQuestionService.getEvaluationQuestionById(req.params.evaluationQuestionId)
            .then(evaluationQuestion => res.status(httpStatus.OK).json(evaluationQuestion))
            .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public createAction(req: Request, res: Response) {
        return this._evaluationQuestionService.addNewEvaluationQuestion(req.body)
            .then(evaluationQuestion => res.status(httpStatus.CREATED).json(evaluationQuestion))
            .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public updateAction(req: Request, res: Response) {
        return this._evaluationQuestionService.updateQuestion(req.params.evaluationQuestionId, req.body)
            .then(evaluationQuestion => res.status(httpStatus.OK).json(evaluationQuestion))
            .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public destroyAction(req: Request, res: Response) {
        return this._evaluationQuestionService.deleteEvaluationQuestion(req.params.evaluationQuestionId)
            .then(evaluationQuestion => res.status(httpStatus.OK).json(evaluationQuestion))
            .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }
}
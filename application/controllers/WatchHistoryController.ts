import * as httpStatus from 'http-status-codes';
import { WatchHistoryService } from "../services/WatchHistoryService";
import { Request, Response } from "express-serve-static-core";

export class WatchHistoryController {

    public constructor(private _watchHistoryService: WatchHistoryService) {
        this.showVideosWatchedByUserAction = this.showVideosWatchedByUserAction.bind(this);
        this.updateWatchedStatusAction = this.updateWatchedStatusAction.bind(this);
        
    }
    public showVideosWatchedByUserAction(req: Request, res: Response) {
        return this._watchHistoryService
            .getTopicsWatchedByUser(req.params.userId)
            .then(videosWatched => { console.log(videosWatched); res.status(httpStatus.OK).json(videosWatched) })
            .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public updateWatchedStatusAction(req: Request, res: Response) {
        return this._watchHistoryService.updateWatchedStatus(req.body)
            .then(() => {
                res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
                res.setHeader('Access-Control-Allow-Methods', 'POST');
                res.status(httpStatus.OK).json('watched status has been saved.')
            })
            .catch(error => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(error));
    }
}
import { Request, Response } from "express";
import { ExamService } from "../services/ExamService";

export class ExamController {
  public constructor(private _examService: ExamService) {
    this.indexAction = this.indexAction.bind(this);
    this.showAction = this.showAction.bind(this);
    this.createAction = this.createAction.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.destroyAction = this.destroyAction.bind(this);
  }

  public indexAction(req: Request, res: Response) {
    return this._examService
      .getAllExams()
      .then(exams => res.send(exams))
      .catch(err => res.send(err));
  }

  public showAction(req: Request, res: Response) {
    return this._examService
      .getExamById(req.params.examId)
      .then(exam => res.send(exam))
      .catch(err => res.send(err));
  }

  public createAction(req: Request, res: Response) {
    return this._examService
      .addExam(req.body)
      .then(msg => res.send(msg))
      .catch(err => res.send(err));
  }

  public updateAction(req: Request, res: Response) {
      return this._examService.updateExam(req.params.examId, req.body)
      .then(msg => res.send(msg))
      .catch(err => res.send(err));
  }

  public destroyAction(req: Request, res: Response) {
      return this._examService.deleteExam(req.params.examId)
      .then(msg => res.send(msg))
      .catch(err => res.send(err));
  }
}

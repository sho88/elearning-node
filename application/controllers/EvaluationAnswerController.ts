import * as httpStatus from "http-status-codes";
import { EvaluationAnswerService } from "../services/EvaluationAnswerService";
import { Request, Response } from "express";


export class EvaluationAnswerController {

    constructor (private readonly _evaluationAnswerService: EvaluationAnswerService){
        this.indexAction = this.indexAction.bind(this);
        this.showAction = this.showAction.bind(this);
        this.createAction = this.createAction.bind(this);
        this.updateAction = this.updateAction.bind(this);
        this.destroyAction = this.destroyAction.bind(this);
    }

    public indexAction(req: Request, res: Response) {
        this._evaluationAnswerService.getEvaluationAnswers()
        .then(evaluationAnswers => res.status(httpStatus.OK).json(evaluationAnswers))
        .catch(err => res.status(httpStatus.NOT_FOUND).json(err));
    }

    public showAction(req: Request, res: Response) {
        this._evaluationAnswerService.getEvaluaionAnswerById(req.params.evaluationAnswerId)
        .then(evaluationAnswer => res.status(httpStatus.OK).json(evaluationAnswer))
        .catch(err => res.status(httpStatus.NOT_FOUND).json(err));
    }

    public createAction(req: Request, res: Response) {
        this._evaluationAnswerService.addEvaluationAnswer(req.body)
        .then(newEvaluationAnswer => res.status(httpStatus.CREATED).json(newEvaluationAnswer))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public updateAction(req: Request, res: Response) {
        this._evaluationAnswerService.updateEvaluationAnswer(req.params.evaluationAnswerId, req.body)
        .then(evaluationAnswer => res.status(httpStatus.OK).json(evaluationAnswer))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public destroyAction(req: Request, res: Response) {
        this._evaluationAnswerService.deleteEvaluationAnswer(req.params.evaluationAnswerId)
        .then(evaluationAnswer => res.status(httpStatus.OK).json(evaluationAnswer))
        .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }
}
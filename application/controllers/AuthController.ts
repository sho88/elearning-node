import * as httpStatus from "http-status-codes";
import { Request, Response } from "express";
import { AuthService } from "../services/AuthService";

export class AuthController {
  constructor(private _authService: AuthService) {
    this.loginAction = this.loginAction.bind(this);
  }

  public loginAction(req: Request, res: Response) {
    this._authService
      .createToken(req.body)
      .then(user => {
        res.cookie('jwt', user.token, { httpOnly: false, secure: false, signed: true });
        res.status(httpStatus.OK).json(user);
      })
      .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
  }

}

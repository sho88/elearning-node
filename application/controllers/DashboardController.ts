import * as httpStatus from "http-status-codes";
import { Request, Response } from "express";
import { UserService } from "../services/UserService";
import { CourseService } from "../services/CourseService";

export class DashboardController {
    public constructor(private _courseService: CourseService) {
        this.indexAction = this.indexAction.bind(this);
    }

    public indexAction(req: Request, res: Response) {
        this._courseService.getAllCourses()
            .then(courses => {
                console.log(courses);
                res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
                res.setHeader("Vary", "Origin");
                res.setHeader('Access-Control-Allow-Credentials', 'true');
                res.setHeader('Access-Control-Allow-Methods', 'GET');
                res.status(httpStatus.OK).json(courses)
            })
            .catch(err => res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err));
    }

    public adminIndexAction(req: Request, res: Response) {
        return res.send("This is the admin dashboard");
    }
}
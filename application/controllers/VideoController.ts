import * as httpStatus from 'http-status-codes';
import { VideoService } from "../services/VideoService";
import { Response, Request } from "express";


export class VideoController {

    public constructor(private _videoService: VideoService) {
        this.indexAction = this.indexAction.bind(this);
        this.showAction = this.showAction.bind(this);
        this.showCourseVideosAction = this.showCourseVideosAction.bind(this)
        this.createAction = this.createAction.bind(this);
        this.createAction = this.createAction.bind(this);
        this.updateAction = this.updateAction.bind(this);
        this.destroyAction = this.destroyAction.bind(this);
    }

    public indexAction(req: Request, res: Response) {
        return this._videoService.getAllVidoes()
            .then(videos => res.status(httpStatus.OK).json(videos))
            .catch(error => res.json(error));
    }

    public showAction(req: Request, res: Response) {
        return this._videoService.getVideoById(req.params.videoId)
            .then(video => res.status(httpStatus.OK).json(video))
            .catch(error => res.json(error));
    }

    public showCourseVideosAction(req: Request, res: Response) {
        return this._videoService.getVideoByCourseId(req.params.courseId)
            .then(videos => res.status(httpStatus.OK).json(videos))
            .catch(error => res.json(error));
    }

    public createAction(req: Request, res: Response) {
        return this._videoService.addVideo(req.body)
            .then(video => res.status(httpStatus.CREATED).json(video))
            .catch(error => res.json(error));
    }

    public updateAction(req: Request, res: Response) {
        return this._videoService.updateVideo(req.params.videoId, req.body)
            .then(video => res.status(httpStatus.OK).json(video))
            .catch(error => res.json(error));
    }

    public destroyAction(req: Request, res: Response) {
        return this._videoService.deleteViideo(req.params.videoId)
            .then(message => res.status(httpStatus.OK).json(message))
            .then(error => res.json(error));
    }

}
import * as config from "config";
import * as express from "express";
import * as passport from "passport";
import * as bodyParser from "body-parser";
import * as nunjucks from "nunjucks";
import * as path from "path";
import * as cookieParser from "cookie-parser";
import { routes } from "./routes";
import { createConnection } from "typeorm";
import * as expressValidator from "express-validator";
import * as httpStatusCodes from 'http-status-codes';
import * as cors from "cors";



/**
 * Used to create a connection to the database.
 * returns: a database connection
 */
const connectToDatabase = async () => {
  const connection = await createConnection();
  return connection;
};

const app: express.Application = express();


const options: cors.CorsOptions = {
  origin: "http://localhost:3000", // this can only be for the frontend application
  allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token", 'Authorization'],
  credentials: true,
  methods: "GET, POST, PATCH, DELETE",
  preflightContinue: false
};

app.use(cors(options));

express.static(__dirname + '/public');
app.set("views", path.resolve(__dirname + "/views"));
app.set("view engine", "njk");
nunjucks.configure(app.get("views"), {
  autoescape: true,
  express: app
});

connectToDatabase();
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(cookieParser(config.get("cookie.key")));
//app.use(cors());


app.use(routes);

app.use((err: express.ErrorRequestHandler, req: express.Request, res: express.Response, next: express.NextFunction) => {
  console.log('Page unavailable at this time; please try again later.');
  return res
    .status(httpStatusCodes.INTERNAL_SERVER_ERROR)
    .send('Service Unavailable');
});

export default app;

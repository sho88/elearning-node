const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const minify = require('gulp-csso');
const scssFilesDirectory = './frontend/scss/**/*.scss';
const jsFilesDirectory = './frontend/js/**/*.js';

function css (cb) {
  gulp.src(scssFilesDirectory)
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('app.min.css'))
    .pipe(minify())
    .pipe(gulp.dest('./public/dist'));

  cb();
}

function js (cb) {
  gulp.src(jsFilesDirectory)
    .pipe(concat('app.min.js'))
    .pipe(minify())
    .pipe(gulp.dest('./public/dist'));
  
  cb();
}

function scssWatch (cb) {
  gulp.watch(scssFilesDirectory, ['css']);
  gulp.watch(jsFilesDirectory, ['js']);

  cb();
}

exports.css = css;
exports.js = js;
exports.scssWatch = scssWatch;

exports.default = gulp.series(css, js);

(function () {
  'use strict';

  const UserModule = () => {
    const getUsers = cb => {
      if (typeof cb === 'function') {
        cb();
      }
    }

    return { getUsers }
  }();

  UserModule.getUsers(() => console.log('Get users triggered.'));
}());

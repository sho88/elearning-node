import {MigrationInterface, QueryRunner} from "typeorm";

export class GroupUserTableMigration1552172137961 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("CREATE TABLE IF NOT EXISTS usergroup ( " +
            "id INT AUTO_INCREMENT, " +
            "user_id INT UNSIGNED NOT NULL, " +
            "groups_id INT NOT NULL, " +
            "PRIMARY KEY (id), " +
            "FOREIGN KEY (user_id) REFERENCES user(id), " +
            "FOREIGN KEY (groups_id) REFERENCES groups(id)" +
            ");"
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("DROP TABLE groupuser");
    }

}

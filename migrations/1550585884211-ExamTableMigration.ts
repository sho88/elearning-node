import {MigrationInterface, QueryRunner} from "typeorm";

export class ExamTableMigration1550585884211 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE exam ADD COLUMN created_at INTEGER; ALTER TABLE exam ADD COLUMN updated_at INTEGER; ALTER TABLE exam ADD COLUMN user_id INTEGER NOT NULL;");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

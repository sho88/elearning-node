import {MigrationInterface, QueryRunner} from "typeorm";

export class GroupTableMigration1552171478313 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("CREATE TABLE IF NOT EXISTS groups (id INT AUTO_INCREMENT, PRIMARY KEY (id));")
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

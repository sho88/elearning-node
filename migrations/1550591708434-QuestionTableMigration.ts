import { MigrationInterface, QueryRunner } from "typeorm";

export class QuestionTableMigration1550591708434 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("ALTER TABLE question ADD COLUMN created_at INTEGER; ALTER TABLE question ADD COLUMN updated_at INTEGER; ALTER TABLE question ADD COLUMN user_id INTEGER NOT NULL;");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

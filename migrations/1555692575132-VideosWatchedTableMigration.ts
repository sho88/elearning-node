import {MigrationInterface, QueryRunner} from "typeorm";

export class VideosWatchedTableMigration1555692575132 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        this.createVideosWatchedTable(queryRunner);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

    public async createVideosWatchedTable(qr: QueryRunner) {
       await qr.query("CREATE TABLE videos_watched ( " +
            "id int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY," +
            " watched_status BOOLEAN NOT NULL" + 
            " watched_duration DECIMAL" +
            " course_id int(11) unsigned NOT NULL, " +
            " user_id int(11) unsigned NOT NULL, " +
            " topic_id int(11) unsigned NOT NULL)");
    }

}

import {MigrationInterface, QueryRunner} from "typeorm";

export class InvitationTableMigration1552170268527 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query(
            "CREATE TABLE IF NOT EXISTS invitation( " +
                "id INT AUTO_INCREMENT, " + 
                "invite_from INT NOT NULL, " +  
                "invite_to VARCHAR(255) NOT NULL, " + 
                "status TINYINT NOT NULL, " +  
                "PRIMARY KEY(id) " +
              ");");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

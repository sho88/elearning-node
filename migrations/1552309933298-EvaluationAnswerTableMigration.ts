import {MigrationInterface, QueryRunner} from "typeorm";
import { query } from "express-validator/check";

export class EvaluationAnswerTableMigration1552309933298 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query(
            "CREATE TABLE IF NOT EXISTS evaluationanswer (" +
            "   id INT AUTO_INCREMENT," +
            "   content VARCHAR(255) NOT NULL," +
            "   created_at INT NOT NULL," + 
            "   user_id INT UNSIGNED NOT NULL," +
            "   eval_question_id INT NOT NULL," +
            "   PRIMARY KEY (id)," +
            "   FOREIGN KEY (user_id) REFERENCES user(id)," +
            "   FOREIGN KEY (eval_question_id) REFERENCES evaluationquestion(id));"
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

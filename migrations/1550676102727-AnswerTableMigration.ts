import {MigrationInterface, QueryRunner} from "typeorm";

export class AnswerTableMigration1550676102727 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query(
        "ALTER TABLE answer ADD COLUMN created_at INTEGER NOT NULL; ALTER TABLE answer ADD COLUMN updated_at INTEGER NOT NULL;ALTER TABLE answer ADD COLUMN user_id INTEGER NOT NULL;");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

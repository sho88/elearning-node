import {MigrationInterface, QueryRunner} from "typeorm";

export class TopicTableMigration1551870834714 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("ALTER TABLE topic ADD COLUMN user_id INTEGER NOT NULL; " +
        "ALTER TABLE topic ADD COLUMN video VARCHAR(255);" + 
        "ALTER TABLE topic ADD COLUMN video_duration VARCHAR(20) NOT NULL;"
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }
    
    
}

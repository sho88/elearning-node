import {MigrationInterface, QueryRunner} from "typeorm";

export class CourseTableMigration1551869976432 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("ALTER TABLE course ADD COLUMN user_id INTEGER;")
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

import {MigrationInterface, QueryRunner} from "typeorm";

export class EvaluationQuestionTable1552306271988 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("CREATE TABLE IF NOT EXISTS evaluationquestion ( " +
        "id INT AUTO_INCREMENT, " +
        "content VARCHAR(255) NOT NULL, " +
        "created_at INT NOT NULL, " +
        "updated_at INT NOT NULL, " +
        "PRIMARY KEY (id));");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

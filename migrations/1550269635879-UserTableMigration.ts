import { MigrationInterface, QueryRunner } from "typeorm";

export class UserTableMigration1550269635879 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    this.changeToNotNull(queryRunner);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}

  public async changeToNotNull(qr: QueryRunner) {
    await qr.query(
        "ALTER TABLE user MODIFY COLUMN password VARCHAR(191) NOT NULL;" +
          " ALTER TABLE user MODIFY COLUMN email_address VARCHAR(191) NOT NULL;" +
          " ALTER TABLE user MODIFY COLUMN telephone_number VARCHAR(191) NOT NULL;" +
          " ALTER TABLE user MODIFY COLUMN created_at int(15) UNSIGNED NOT NULL;" +
          " ALTER TABLE user MODIFY COLUMN updated_at int(15) UNSIGNED NOT NULL;"
      );
  }
}
